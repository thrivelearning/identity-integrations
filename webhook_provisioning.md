# Provisioning via Webhook
Sending Identity Management Events via Webhook

## About
This document details the mechanism by which user lifecycle events can be sent to THRIVE via HTTPS using JSON payloads in order to achieve an event driven identity management integration.

View the OpenAPI version of these docs: [OpenAPI Docs](https://api-docs.learndev.link/)

## Headers

**Authorisation**

All header requests are sent in Basic Auth consisting of your accounts Username (Tenant ID) and Password (API Key).

* **Authorisation type:** Basic Auth
* **Username:** Your website Tenant ID (eu-west-000000 please contact support for your tenant).
* **Password:** Your generated API Key

**Content-Type**

The Content-Type header should be set to `application/json`.

## Payload

1. As a user reference field, the individual identified in **managerRef** must have already been created on THRIVE before being passed here. Where the managerRef cannot be linked to a valid and active user account the relationship will not be set.
2. Where languageCode is sent this must be one of the configured language codes supported for your THRIVE instance
