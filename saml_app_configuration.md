# SAML App Configuration
## Adding Thrive LXP as a SAML Service Provider

## About
This document provides the required information for adding the Thrive LXP as a required

## Configuration Details
### IDP Metadata
In order for us to add your identity provider to our authentication system, we will need a metadata file including your public certificate from your IDP.

We also request information about the date on which your certificate is due to expire for our record keeping.

### Service Provider

The Thrive LXP will need to be added as a service provider (sometimes referred to as a “Relying Party”) for your IDP. The following information will be required for this:

### Signing Certificate
The signing certificate for your LXP is as follows:

```
Detail to be provided on implementation
```

### ACS URL / Endpoint URL
https://subdomain-lxp.auth.eu-west-2.amazoncognito.com/saml2/idpresponse

### Entity ID
urn:amazon:cognito:sp:eu-west-2_1foobarw


