# Gsuite SAML App Configuration
## Adding Thrive LXP as a Custom SAML App on GSuite

## About
This document outlines the steps for adding an instance of the Thrive LXP as a SAML Application through the GSuite administration panel.

### Table Of Contents

1. [IDP Metadata](#markdown-header-provide-idp-metadata)
2. [Information From Thrive](#markdown-header-information-from-thrive)
3. [Adding Thrive as a Service Provider](#markdown-header-adding-thrive-as-a-service-provider)
4. [Enable Service](#markdown-header-enable-service)


## Instructions

### Provide IDP Metadata
Access the SAML service management page on GSuite by navigating to “Apps” > “SAML Apps”  in the admin panel.

Click the “Add” button and select “SETUP MY OWN CUSTOM APP”.

![gsuitecustom](images/gsuitecustom.png)

Download the IDP Metadata file under “Option 2”.

![gsuitedownload](images/gsuitedownload.png)

Send the XML file through to your contact at Thrive. 

*Note that cancelling the dialogue at this stage does not invalidate the IDP metadata.*

### Information from Thrive

Your contact at Thrive will provide the following information:
  
- ACS URL
- Entity ID

### Adding Thrive as a Service Provider

Return to the SAML service management page and then select “Add” and “SETUP MY OWN CUSTOM APP” once again. Click next until you reach step 3.

#### Gsuite Step 3
You will be asked to provide basic information for the application name. *Note that the description and logo can both be added later if needed.*

#### Gsuite Step 4
Enter the ACS URL and Entity ID provided by Thrive. All other fields can be left as default.

![gsuiteacs](images/gsuiteacs.png)

#### Gsuite Step 5

| APPLICATION ATTRIBUTE  |  CATEGORY | USER FIELD  |
|---|---|---|
|  givenName |  Basic Information |First Name   |
|  surname | Basic Information  |  Last Name |
| mail  |  Basic Information | Primary Email  |

### Enable Service
Before any user can log in from your GSuite account, you will need to enable the service. You can do this from the SAML service management page by clicking on the right hand “three dot” menu and selecting “ON for everyone”.

![gsuiteenable](images/gsuiteenable.png)

