# OneLogin App Configuration

## Adding Thrive as an application in OneLogin

## About

This document outlines the steps for adding an instance of the Thrive application in OneLogin.


### Instructions

- In the OneLogin Administration portal, go to *Applications*, click on *Add App* and type *thrive* in the search box:

![OneLoginAddApplication](images/OneLogin-add-application.png)  

- Select the *Thrive* application. You can change the logo or the *Visible in portal* setting here if required but these can also be changed later:

![OneLoginThriveApp](images/OneLogin-thrive-app.png)  

- Click on *Save* to add the application to your portal.


#### SSO Configuration

- Select the *Configuration* tab and fill in *SAML Audience*, *ACS (Consumer) URL Validator*, *ACS (Consumer) URL* and *Login URL* as provided by your contact at Thrive under *Application details*:

![OneLoginAppDetails](images/OneLogin-app-details.png)  

- Click on *More Actions* at the top right and *SAML Metadata*. This will download an XML file which you need to send to your contact at Thrive so we can configure your site for SSO:

![OneLoginSAMLMetadata](images/OneLogin-saml-metadata.png)  


#### Provisioning Configuration

- If using SCIM provisioning, add the *SCIM Base URL* and *SCIM Bearer Token* in the *API Connection* section as provided by your contact at Thrive and click on the *Enable* button:

![OneLoginAPIConnection](images/OneLogin-api-connection.png)  

- In the *Parameters* tab you can see the attributes that will be populated in Thrive by default if using provisioning:

![OneLoginParameters](images/OneLogin-parameters.png)  

If you want to add any further attributes to this list please speak to your contact at Thrive first to ensure a custom field and mapping is created for it on our side.

When adding additional attributes to the list you must ensure the *Include in User Provisioning* checkbox is selected before clicking on *Save*:

![OneLoginNewField](images/OneLogin-new-field.png)  

Select the appropriate source attribute in the *Value* drop down and click on *Save*

![OneLoginEditField](images/OneLogin-edit-field.png)  

Go to the *Configuration* tab and add the parameter to the SCIM JSON template as shown below:

![OneLoginEditField](images/OneLogin-json-template.png)