# Azure App Configuration

## Adding Thrive LXP as an Application on Azure AD

## About
This document outlines the steps for adding an instance of the THRIVE LXP as a new application on Azure AD.

### Instructions for SSO

https://docs.microsoft.com/en-us/azure/active-directory/saas-apps/thrive-lxp-tutorial

### Instructions for Provsioning

https://docs.microsoft.com/en-us/azure/active-directory/saas-apps/thrive-lxp-provisioning-tutorial

**Additional attributes**

Please note when adding additional attributes to the sync by editing the *ThriveLxp User Attributes* list, the following extension attribute names can be used:

```
urn:ietf:params:scim:schemas:extension:enterprise:2.0:User:employeeNumber
urn:ietf:params:scim:schemas:extension:enterprise:2.0:User:costCenter
urn:ietf:params:scim:schemas:extension:enterprise:2.0:User:organization
urn:ietf:params:scim:schemas:extension:enterprise:2.0:User:division
urn:ietf:params:scim:schemas:extension:enterprise:2.0:User:department
```

If these are unsuitable then custom attribute names can be created, provided they are prefixed with the core namespace rather than extension namespace. For example to add an attribute called `country` name it like this:

```
urn:ietf:params:scim:schemas:core:2.0:User:country
```