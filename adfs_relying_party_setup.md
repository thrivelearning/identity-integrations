# ADFS Relying Party Setup
## Adding Thrive LXP as a Relying Party on ADFS

### About
This document outlines the steps for adding an instance of the Thrive LXP as a new relying party on Microsoft ADFS.

### Table Of Contents

1. [IDP Metadata](#markdown-header-provide-idp-metadata)
2. [Information From Thrive](#markdown-header-information-from-thrive)
3. [Adding Thrive as a Relying Party](#markdown-header-adding-thrive-as-a-relying-party)
4. [Adding Claim Rules](#markdown-header-adding-claim-rules)
5. [Testing](#markdown-header-testing)

### First Steps

#### Provide IDP Metadata
In order to add your ADFS server as an identity provider, the Thrive team will need your server’s public metadata file. Send this to your contact at Thrive. 

This can be found on your ADFS server, usually at the following address:
```
   https://<servername>/FederationMetadata/2007-06/FederationMetadata.xml
```

Please provide either this URL or the file itself to the team at Thrive.

#### Information From Thrive
Your contact at Thrive will provide the following information:

- Endpoint URL
- Entity ID

### Instructions
#### Adding Thrive as a Relying Party

Go to Trust Relationships > Relying Party Trusts > Add relying party trusts.  Follow these steps to complete the wizard:
1. Select Enter data about the relying party manually.
  
2. Enter a display name for the relying party configuration.
  
3. On the next screen, do not configure a certificate.
  
4. Enable support for SAML 2.0 SSO service URL.
  
5. Add the Entity ID provided to you by Thrive as the relying party trust identifier.
  
6. Configure the SAML POST binding.  The SAML 2.0 post-binding endpoint should be set to the Endpoint URL provided to you by Thrive.
  
7. Select Permit all users to access this relying party.
  
8. Choose Finish.
  

After you’ve finished the ADFS configuration, navigate to Trust Relationships > Relying Party Trusts.  The screen should look similar to the following screenshot:

![ADFSPartyTrusts](images/ADFSPartyTrusts.png)

#### Adding Claim Rules
In a SAML federation, the IdP can pass various attributes about the user, the authentication method, or other points of context to the service provider in the form of SAML attributes.

In ADFS, claim rules are used to assemble these required attributes using a combination of Active Directory lookups, simple transformations, and regular expression-based custom rules.  

##### Standard Mappings

The table below outlines the standard attribute mappings for the application when integrating with ADFS.

| LDAP ATTRIBUTE      | OUTGOING CLAIM  | URI |
| ----------- | ----------- | ----------- |
| Windows account name      | Name ID       | http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname |
| E-mail Address   | E-Mail Address        | http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress |
| Display Name | Name | http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name |
| Given Name      | Given Name       | http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname |
| Surname   | Surname        | http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname |

##### Adding Claims

The **Edit Claim Rules** window should already be open.  If it isn’t, select your relying party trust from the **Trust Relationships > Relying Party Trusts** screen, then choose **Edit Claim rules**.

![ADFSClaimRules](images/ADFSClaimRules.png)

On the **Choose Rule Type** page, select **Send LDAP Attributes as Claims**.
On the **Configure Claim Rule** page, type the following settings, then choose **OK** as shown in the following screenshot:

![ADFSClaimRules2](images/ADFSClaimRules2.png)

The Name ID Claim Rule should look like this:

![ADFSClaimRules3](images/ADFSClaimRules3.png)

Repeat the same steps for the E-mail Claim.  The E-Mail Claim Rule should look like this:

![ADFSClaimRules4](images/ADFSClaimRules4.png)

Repeat the same steps for the Display Name, Given Name and Surname Claims.

#### Testing

Once these steps have been completed and the relying party enabled on your ADFS Server, please inform your contact at Thrive.

We will set up a test gateway for your application and provide you with a URL from which to test it using a valid Active Directory login.

##### Troubleshooting
Should there be any issues we can organise a screen share session to diagnose and resolve the problem together.









