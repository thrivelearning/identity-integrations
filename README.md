# Identity Integrations

## Introduction
THRIVE incorporates a number of industry standard integrations to support customers looking to integrate the solution into their corporate infrastructure in various ways. This document outlines the most common integrations and standards we see customers taking advantage of.

## Identity Management

### What is Identity Management?
As cloud technologies become increasingly popular, organisations need processes and systems in place to securely handle the registration and authentication of user profiles across systems. 
  
We commonly find Microsoft Azure AD and Google GSuite are the most popular choices for handling these challenges, but the standards we support are commonly incorporated into other identity management systems including OKTA, ADFS and SAML.

## Provisions

The preferred method of automatically importing identities is through SCIM, details of this process can be found below. Where SCIM is not a viable solution, we can utilise a web hook system to achieve a similar effect.

### SCIM 2

The second version of the [System for Cross-domain Identity Management](http://www.simplecloud.info/) was launched in 2015 and has since seen rapid adoption across identity management systems. 
  
It is the de facto open standard for provisioning identities in the cloud and incorporates full support for managing custom profile fields as well as an organisational hierarchy for users.
  
We recommend the use of SCIM 2 because it offers the most straightforward process for implementing an identity provisioning integration.

### Web Hooks
Where an identity management system does not offer support for the SCIM 2 standard, the next best option is usually to utilise a web hook system whereby your system can notify our platform of events in the lifecycle of your users. This allows us to keep them up to date in real time.
  
We have a dedicated service for handling webhook requests, and we can utilise this to transform event information from your system into updates for users. 
  
An initial full user sync is required using the manual CSV import, after which the event notifications should keep the user records up to date.
  
While this is possible, a web hook based solution requires significant custom development and will slow the implementation process.

  
## Single Sign On
  
Single sign on technologies allow your users to access a range of services for your organisation, while also giving you the ability to enforce whatever security policies you have in place for your organisation.
  
### SAML 2  
  
This is the most commonly requested standard by far, which is widely supported by a range of identity management systems including Microsoft Azure AD, Google GSuite, Microsoft ADFS, OKTA, PingID and Shibboleth.
  
We will share specific documentation to guide you through the setup, and offer direct contact between technical teams to ensure the setup can be carried out as quickly as possible. We typically see no more than a week being required to achieve a SAML integration.

### OAUTH
  
We have initial support for OAUTH within our system, and if you are using an identity management system which does not have support for SAML 2 this can be a good alternative.  

## Application Guides

For more technical guides of how to handle Identity Integration, follow the corresponding link below: 

- [ADFS Relying Party Setup](adfs_relying_party_setup.md)
- [GSuite SAML App Setup](gsuite_saml_app_configuration.md)
- [SAML Service Setup](saml_app_configuration.md)
- [Azure App Configuration](azure_app_configuration.md)
- [Okta App Configuration](okta_app_configuration.md)




