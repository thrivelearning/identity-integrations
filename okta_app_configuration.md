# Okta App Configuration

## Adding Thrive LXP as an application in Okta

## About

This document outlines the steps for adding an instance of the Thrive LXP as a new application in Okta.


### Instructions

- In Okta, go to *Applications* and click on *Add Application*

![OktaApplications](images/Okta-add-application.png)


- Type *Thrive LXP* in the search box and then click on *Add*


![OktaThrive](images/Okta-Thrive.png)


- You can change the *Application Visibility* settings here if required. Thrive only supports a SP initiated flow so you might want to hide the app and create a bookmark to simulate IdP initiated flow by following this process:
https://help.okta.com/en/prod/Content/Topics/Apps/Apps_Bookmark_App.htm

When configured in this way users will be able access Thrive directly from the Okta Dashboard. Click on *Done* when complete. 

![OktaThriveSettings](images/Okta-Thrive-settings.png)


- Navigate to the *Sign On* and *Provisioning* tabs and follow the instructions within the app to configure it.
