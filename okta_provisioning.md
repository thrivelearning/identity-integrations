# Okta Provisioning

## Configuring User Provisioning in the Thrive LXP Okta App

## About

This document outlines the steps for configuring User Provisioning in the Thrive LXP Okta App.


## Table of Contents

1. [Features](#markdown-header-features)
2. [Requirements](#markdown-header-requirements)
3. [Step-By-Step Configuration Instructions](#markdown-header-step-by-step-config)


### Features

The following provisioning features are supported:

- **Push New Users**

New users created through Okta will also be created in the Thrive LXP.

- **Push Profile Updates**

Updates made to the user's profile through Okta will be pushed to Thrive LXP.

- **Push User Deactivation**

Deactivating the user or disabling the user's access to the application through Okta will deactivate the user in the Thrive LXP.

*Note*: Deactivating a user means removing access to login but maintaining the user's data in Thrive as an inactive user.

- **Reactivate Users**

User accounts can be reactivated in the Thrive LXP.


### Requirements

Before you configure provisioning for Thrive LXP you must reach out to the support team to obtain an API token (support@thrivelearning.com).


### Step-By-Step Configuration Instructions

#### Users

- Go to the *Provisioning* tab and click on the *Configure API Integration* button

![OktaIntegration](images/Okta-configure-integration.png)

- Tick *Enable API integration* and enter the API token and base URL provided by Thrive
- Click on the *Test API Credentials* button to verify the token works before clicking on *Save*

![OktaCredentials](images/Okta-test-credentials.png)

- Click on the *Edit* button and select the required features (this will usually be all of them, i.e. *Create Users*, *Update User Attributes* and *Deactivate Users*)

![OktaFeatures](images/Okta-features.png)

- Click on *Save*

These are the attributes that are supported by default:

![OktaAttributes](images/Okta-attributes.png)

If you wish to sync additional attributes then add them to this list and let your contact at Thrive know the *Value*. 
**Please Note** when creating your additional attributes the *External namespace* should be either of the SCIM core or extension namespaces, i.e.
```
urn:ietf:params:scim:schemas:core:2.0:User
urn:ietf:params:scim:schemas:extension:enterprise:2.0:User
```
So creating an attribute called `Division` in the core namespace would look like this:

![OktaAdditionalAttribute](images/Okta-additional-attribute.png)

Thrive will need to create custom fields and mappings in your database to store those attributes. 


A note on **Start Date** - this field in Thrive LXP will be populated with the date that the user is created on our side. If you would like to populate this with a different date from an Okta attribute please ensure that the date in Okta is in the format *yyyy-MM-dd* and let your contact at Thrive know the *Value* so we can create a mapping for it. 

If your source attribute has a different date format you can convert it using an expression mapping, documentation here:
https://developer.okta.com/docs/reference/okta-expression-language/


#### Groups

If you wish to sync groups to the THRIVE application then select *Push Groups* and add any groups you want to sync. The name of the group will be mapped to the *Organisation* field in Thrive LXP.
